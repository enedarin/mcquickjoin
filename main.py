from time import sleep
from _thread import start_new_thread

from mcstatus import MinecraftServer
from colorama import init, Fore, Style, AnsiToWin32
import mouse

init(autoreset=True, convert=True)
stream = AnsiToWin32(sys.stderr).stream


DEFAULT_SERVER = "lotr.g.akliz.net"
slots_are_open = False
pings = 0


def update_whether_slots_are_open(server):
    global slots_are_open
    global pings
    query = server.query()
    max = query.players.max
    online = query.players.online
    pings += 1
    print(f"{Style.DIM}#{pings}\t{Style.RESET_ALL}{Style.BRIGHT}slots: {Fore.YELLOW}{online}{Fore.GREEN}/{Fore.YELLOW}{max}", file=stream)

    if max - online > 0:
        slots_are_open = True


def show_help():
    print(
        f"{Style.DIM}You will type in the address of the Minecraft server you wish to join once a slot is available.\n"
        f"If you press enter without an address, it will use {DEFAULT_SERVER}.\n"
        "After providing the address, place your cursor in your game's menu above the server you wish to join.\n"
        "As soon as a slot opens, it will double click where your cursor is to join the server.\n"
        "Press CRTL+C any time to cancel.\n",
        file=stream
    )


def countdown():
    for countdown in range(1, 4)[::-1]:
        print(f"{Fore.YELLOW}{countdown}", file=stream)
        sleep(1)


def main():
    global slots_are_open
    show_help()
    print(f"{Fore.GREEN}address [{Fore.YELLOW}{Style.BRIGHT}{DEFAULT_SERVER}{Style.RESET_ALL}{Fore.GREEN}]: ", file=stream)
    address = input()
    if address == "":
        address = DEFAULT_SERVER

    server = MinecraftServer.lookup(address)
    try_again = True
    while try_again:
        countdown()
        while not slots_are_open:
            start_new_thread(update_whether_slots_are_open, (server,))
            sleep(0.1)

        mouse.double_click()
        print(f'{Style.BRIGHT}{Fore.MAGENTA}*CLICK*', file=stream)
        sleep(2)
        print(f"\n{Fore.GREEN}retry? ({Fore.YELLOW}{Style.BRIGHT}y{Fore.GREEN}/{Fore.YELLOW}n{Style.RESET_ALL}{Fore.GREEN}): ", file=stream)
        user_retry = input()
        if user_retry != "y":
            try_again = False
        slots_are_open = False


if __name__ == "__main__":
    main()
